const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];
document.addEventListener("DOMContentLoaded", function () {
  const div = document.getElementById("root");
  console.log(div);

  const ul = document.createElement("ul");

  books.forEach((book) => {
    try {
      if (!book.author || !book.name || !book.price) {
        throw new Error( `Помилка: у вас не має властивості price. ${JSON.stringify(book)}`);
      }

      const li = document.createElement("li");
      li.textContent = `${book.author} ${book.name} ${book.price}`;

      ul.append(li);
    } catch (error) {
      console.error(error);
    }
  });

  root.append(ul);
});


// function validateBook(book) {
//   if (!book.author  || !book.name || !book.price) {
//     throw new Error(`Помилка: Необхідно: author, name, price. Не з'являється у списку: ${JSON.stringify(book)}`);
//   }
// }

// function createBookList(books) {
//   const ul = document.createElement("ul");

//   books.forEach(book => {
//     try {
//       validateBook(book);
//       const li = document.createElement("li");
//       li.textContent = `${book.author} - ${book.name} - ${book.price} грн`;
//       ul.append(li);
//     } catch (error) {
//       console.error(error);
//     }
//   });

//   return ul;
// }

// document.addEventListener("DOMContentLoaded", function() {
//   const rootDiv = document.getElementById("root");
//   const bookList = createBookList(books);
//   rootDiv.appendChild(bookList);
// });